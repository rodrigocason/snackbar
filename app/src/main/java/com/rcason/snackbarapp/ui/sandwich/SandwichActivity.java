package com.rcason.snackbarapp.ui.sandwich;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ActivitySandwichBinding;
import com.rcason.snackbarapp.domain.entity.Sandwich;
import com.rcason.snackbarapp.ui.base.BaseActivity;
import com.rcason.snackbarapp.ui.editsandwich.EditSandwichActivity;
import com.rcason.snackbarapp.ui.orderlist.OrderListActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class SandwichActivity extends BaseActivity<ActivitySandwichBinding, SandwichViewModel> implements SandwichNavigator {

    @Inject
    SandwichViewModel mSandwichViewModel;

    ActivitySandwichBinding mActivitySandwichBinding;

    List<Sandwich> mList = new ArrayList<>();
    private SandwichAdapter mAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, SandwichActivity.class);
        return intent;
    }

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_sandwich;
    }

    @Override
    public SandwichViewModel getViewModel() {
        return mSandwichViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataBinding();
        setToolbar();
        setUpRecyclerView();
        prepareData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                //Intent intent = new Intent(CurrentActivity.this, MainActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                //startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initDataBinding() {
        mActivitySandwichBinding = getViewDataBinding();
        mActivitySandwichBinding.setViewModel(mSandwichViewModel);
        mSandwichViewModel.setNavigator(this);
    }

    private void setToolbar() {
        setSupportActionBar(mActivitySandwichBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mActivitySandwichBinding.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        mActivitySandwichBinding.toolbar.setTitleTextColor(getColor(R.color.white));
    }

    // set up the list of user with recycler view
    private void setUpRecyclerView() {

        mAdapter = new SandwichAdapter(this, mList, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, Sandwich sandwich) {

                AlertDialog alertDialog = new AlertDialog.Builder(SandwichActivity.this).create();
                alertDialog.setTitle(sandwich.getName());
                alertDialog.setMessage("Alert message to be shown");
                alertDialog.setCancelable(false);
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Adicionar ítens ao Sanduíche",
                        (dialog, which) -> openEditSandwichActivity(sandwich));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Colocar sanduíche no carrinho",
                        (dialog, which) -> mSandwichViewModel.saveOrder(sandwich.getId()));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancelar",
                        (dialog, which) -> dialog.dismiss());
                alertDialog.show();
            }
        });
        mActivitySandwichBinding.rvSandwiches.setAdapter(mAdapter);
        mActivitySandwichBinding.rvSandwiches.setLayoutManager(new LinearLayoutManager(this));
    }

    private void prepareData() {
        ViewModelProviders.of(this)
                .get(SandwichViewModel.class)
                .getSandwiches()
                .observe(this, sandwiches -> {
                    mList.addAll(sandwiches);
                    mAdapter.notifyDataSetChanged();
                });
    }

    private void openEditSandwichActivity(Sandwich sandwich) {
        Intent intent = EditSandwichActivity.newIntent(SandwichActivity.this, sandwich);
        startActivity(intent);
    }

    private void openOrderActivity() {
        Intent intent = OrderListActivity.newIntent(SandwichActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void sendMessage(String title, String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        openOrderActivity();

    }
}
