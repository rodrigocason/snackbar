package com.rcason.snackbarapp.ui.editsandwich;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ItemIngredientsBinding;
import com.rcason.snackbarapp.domain.entity.Ingredient;
import com.rcason.snackbarapp.domain.entity.Sandwich;
import com.rcason.snackbarapp.util.Fontawesome;

import java.util.List;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public class EditSandwichAdapter extends RecyclerView.Adapter<EditSandwichAdapter.BindingHolder> {

    Sandwich mSandwich;
    List<Ingredient> mList;

    private Context mContext;
    private OnClickListener mOnClickListener;

    public EditSandwichAdapter(Context context, Sandwich sandwich, List<Ingredient> list, OnClickListener onClickListener) {
        this.mContext = context;
        this.mSandwich = sandwich;
        this.mList = list;
        this.mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public BindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemIngredientsBinding itemIngredientsBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_ingredients,
                        parent,
                        false);


        return new EditSandwichAdapter.BindingHolder(itemIngredientsBinding, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull BindingHolder holder, int position) {
        Ingredient ingredient = mList.get(position);
        ItemIngredientsBinding itemIngredientsBinding = holder.binding;
        itemIngredientsBinding.setViewModel(new ItemEditSandwichViewModel(mContext, mSandwich, ingredient));

        itemIngredientsBinding.tvCount.setText("0");

        itemIngredientsBinding.btnMinus.setTag(position);
        itemIngredientsBinding.btnMinus.setImageDrawable(Fontawesome.getIcon(mContext, Fontawesome.ICON_MINUS_CIRCLE, 30F, Color.argb(255, 43, 255, 130)));
        itemIngredientsBinding.btnMinus.setOnClickListener(v -> {
            if (Integer.valueOf(itemIngredientsBinding.tvCount.getText().toString()) > 0) {
                Integer pos = (Integer) itemIngredientsBinding.btnMinus.getTag();
                mOnClickListener.onMinusClick(pos, mList.get(pos));
                itemIngredientsBinding.tvCount.setText(String.valueOf(Integer.valueOf(itemIngredientsBinding.tvCount.getText().toString()) - 1));
            }
        });

        itemIngredientsBinding.btnPlus.setTag(position);
        itemIngredientsBinding.btnPlus.setBackgroundColor(mContext.getColor(R.color.white));
        itemIngredientsBinding.btnPlus.setImageDrawable(Fontawesome.getIcon(mContext, Fontawesome.ICON_PLUS_CIRCLE, 30F, Color.argb(255, 43, 255, 43)));
        itemIngredientsBinding.btnPlus.setOnClickListener(v -> {
            Integer pos = (Integer) itemIngredientsBinding.btnPlus.getTag();
            mOnClickListener.onPlusClick(pos, mList.get(pos));
            itemIngredientsBinding.tvCount.setText(String.valueOf(Integer.valueOf(itemIngredientsBinding.tvCount.getText().toString()) + 1));
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        final ItemIngredientsBinding binding;

        private final Context context;

        public BindingHolder(ItemIngredientsBinding binding, Context context) {
            super(binding.getRoot());

            this.binding = binding;
            this.context = context;
        }
    }
    void setOnClickListener(final OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onMinusClick(int position, Ingredient ingredient);
        void onPlusClick(int position, Ingredient ingredient);
    }
}
