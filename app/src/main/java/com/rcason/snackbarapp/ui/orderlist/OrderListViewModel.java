package com.rcason.snackbarapp.ui.orderlist;

import android.arch.lifecycle.MutableLiveData;

import com.rcason.snackbarapp.data.remote.ApiFactory;
import com.rcason.snackbarapp.data.remote.ingredient.IngredientResponse;
import com.rcason.snackbarapp.data.remote.ingredient.IngredientService;
import com.rcason.snackbarapp.data.remote.orderlist.OrderResponse;
import com.rcason.snackbarapp.data.remote.orderlist.OrderService;
import com.rcason.snackbarapp.data.remote.sandwich.SandwichResponse;
import com.rcason.snackbarapp.data.remote.sandwich.SandwichService;
import com.rcason.snackbarapp.domain.entity.Ingredient;
import com.rcason.snackbarapp.domain.entity.Order;
import com.rcason.snackbarapp.domain.entity.Sandwich;
import com.rcason.snackbarapp.ui.base.BaseViewModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.schedulers.Schedulers;

import static io.reactivex.Observable.empty;
import static io.reactivex.Observable.fromIterable;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class OrderListViewModel extends BaseViewModel<OrderListNavigator> {
    public OrderListViewModel() {

    }

    private MutableLiveData<List<Order>> mOrdersList;

    MutableLiveData<List<Order>> getOrders() {
        if (mOrdersList == null) {
            mOrdersList = new MutableLiveData<>();
            fetchOrders();
        }
        return mOrdersList;
    }

    private void fetchOrders() {

        setIsLoading(true);

        zip().onErrorResumeNext(handleOrderListError()).subscribe(getListOrders());

        setIsLoading(false);
    }

    private Observable<ZippedResponse> zip() {
        return Observable.zip(getOrdersListRequest(), getListOfIngredientsRequest(), getSandwichesListRequest(), new Function3<List<OrderResponse>, List<IngredientResponse>, List<SandwichResponse>, ZippedResponse>() {

            @Override
            public ZippedResponse apply(@NonNull List<OrderResponse> OrderResponses, @NonNull List<IngredientResponse> IngredientResponses, @NonNull List<SandwichResponse> SandwichResponses) throws Exception {
                return new ZippedResponse(OrderResponses, IngredientResponses, SandwichResponses);
            }
        });
    }

    private Observable<List<OrderResponse>> getOrdersListRequest() {
        return ApiFactory.createService(OrderService.class).getAllOrders()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<List<SandwichResponse>> getSandwichesListRequest() {
        return ApiFactory.createService(SandwichService.class).getAllSandwiches()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<List<IngredientResponse>> getListOfIngredientsRequest() {
        return ApiFactory.createService(IngredientService.class).getAllIngredients()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<ZippedResponse> getListOrders() {
        return new Consumer<ZippedResponse>() {
            @Override
            public void accept(ZippedResponse response) throws Exception {
                final HashMap<Integer, Ingredient> hashMapIngredients = fromIterable(response.getIngredientResponses()).collectInto(new HashMap<Integer, Ingredient>(), new BiConsumer<HashMap<Integer, Ingredient>, IngredientResponse>() {

                    @Override
                    public void accept(HashMap<Integer, Ingredient> map, IngredientResponse i) throws Exception {
                        map.put(i.id, new Ingredient(i.id, i.name, new BigDecimal(i.price.toString()), i.image));
                    }

                }).blockingGet();
                final HashMap<Integer, Sandwich> hashMapSandwiches = fromIterable(response.getSandwichResponses()).collectInto(new HashMap<Integer, Sandwich>(), new BiConsumer<HashMap<Integer, Sandwich>, SandwichResponse>() {

                    @Override
                    public void accept(HashMap<Integer, Sandwich> map, SandwichResponse sandwichResponse) throws Exception {
                        final Sandwich sandwich = new Sandwich(sandwichResponse.id, sandwichResponse.name, Collections.emptyList(), sandwichResponse.image);

                        fromIterable(sandwichResponse.ingredients).blockingForEach(new Consumer<Integer>() {

                            @Override
                            public void accept(Integer id) throws Exception {
                                Ingredient ingredient = hashMapIngredients.get(id);
                                sandwich.addIngredient(ingredient);
                            }

                        });

                        map.put(sandwichResponse.id, sandwich);
                    }

                }).blockingGet();

                List<Order> result = fromIterable(response.getOrderResponses()).collectInto(new ArrayList<Order>(), new BiConsumer<ArrayList<Order>, OrderResponse>() {
                    @Override
                    public void accept(ArrayList<Order> orders, OrderResponse orderResponse) throws Exception {
                        Sandwich sandwich = hashMapSandwiches.get(orderResponse.id_sandwich);

                        Order order = new Order();
                        order.setSandwich(sandwich);
                        order.setId(orderResponse.id);

                        for (int i = 0; i < orderResponse.extras.size(); i++) {
                            int id = orderResponse.extras.get(i);
                            order.addIngredient(hashMapIngredients.get(id));
                        }

                        orders.add(order);
                    }

                }).blockingGet();

                mOrdersList.setValue(result);
            }
        };
    }

    private Function<Throwable, ObservableSource<? extends ZippedResponse>> handleOrderListError() {
        return new Function<Throwable, ObservableSource<? extends ZippedResponse>>() {
            @Override
            public ObservableSource<? extends ZippedResponse> apply(@NonNull Throwable throwable) throws Exception {
                getNavigator().sendMessage("Erro!", "Erro ao carregar pedidos.");
                return empty();
            }
        };
    }

    private static class ZippedResponse {

        private List<OrderResponse> OrderResponses;
        private List<IngredientResponse> IngredientResponses;
        private List<SandwichResponse> SandwichResponses;

        public ZippedResponse(List<OrderResponse> OrderResponses, List<IngredientResponse> IngredientResponses, List<SandwichResponse> SandwichResponses) {
            this.OrderResponses = OrderResponses;
            this.IngredientResponses = IngredientResponses;
            this.SandwichResponses = SandwichResponses;
        }

        public List<OrderResponse> getOrderResponses() {
            return OrderResponses;
        }

        public List<IngredientResponse> getIngredientResponses() {
            return IngredientResponses;
        }

        public List<SandwichResponse> getSandwichResponses() {
            return SandwichResponses;
        }
    }
}
