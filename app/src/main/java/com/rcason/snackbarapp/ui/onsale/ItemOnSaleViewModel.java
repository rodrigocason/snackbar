package com.rcason.snackbarapp.ui.onsale;

import android.content.Context;
import android.databinding.BaseObservable;

import com.rcason.snackbarapp.domain.entity.OnSaleItem;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class ItemOnSaleViewModel extends BaseObservable {

    private OnSaleItem mOnSaleItem;

    private Context mContext;

    public ItemOnSaleViewModel(Context context, OnSaleItem onSaleItem) {
        this.mContext = context;
        this.mOnSaleItem = onSaleItem;
    }

    public String getName() {
        return mOnSaleItem.name;
    }

    public String getDescription() {
        return mOnSaleItem.description;
    }

    public void setonSaleItem(OnSaleItem onSaleItem) {
        this.mOnSaleItem = onSaleItem;
        notifyChange();
    }
}
