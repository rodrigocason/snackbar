package com.rcason.snackbarapp.ui.editsandwich;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public interface EditSandwichNavigator {
    void sendMessage(String title, String message);
}
