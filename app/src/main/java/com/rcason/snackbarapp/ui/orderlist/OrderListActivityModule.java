package com.rcason.snackbarapp.ui.orderlist;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rodrigo on 06/06/2018.
 */

@Module
public class OrderListActivityModule {

    @Provides
    OrderListViewModel provideOrderListViewModel() {
        return new OrderListViewModel();
    }
}
