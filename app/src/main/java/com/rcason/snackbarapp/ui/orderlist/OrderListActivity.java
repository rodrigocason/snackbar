package com.rcason.snackbarapp.ui.orderlist;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;

import com.rcason.snackbarapp.BR;
import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ActivityOrderListBinding;
import com.rcason.snackbarapp.domain.entity.Order;
import com.rcason.snackbarapp.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class OrderListActivity extends BaseActivity<ActivityOrderListBinding, OrderListViewModel> implements OrderListNavigator {

    @Inject
    OrderListViewModel mOrderListViewModel;

    ActivityOrderListBinding mActivityOrderListBinding;

    List<Order> mOrderList = new ArrayList<>();
    OrderListAdapter mAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, OrderListActivity.class);
        return intent;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_order_list;
    }

    @Override
    public OrderListViewModel getViewModel() {
        return mOrderListViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataBinding();
        setToolbar();
        setUpRecyclerView();
        prepareData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initDataBinding() {
        mActivityOrderListBinding = getViewDataBinding();
        mActivityOrderListBinding.setViewModel(mOrderListViewModel);
        mOrderListViewModel.setNavigator(this);
    }

    private void setToolbar() {
        setSupportActionBar(mActivityOrderListBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mActivityOrderListBinding.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        mActivityOrderListBinding.toolbar.setTitleTextColor(getColor(R.color.white));
    }

    private void setUpRecyclerView() {
        mAdapter = new OrderListAdapter(this, mOrderList);
        mActivityOrderListBinding.rvOrders.setAdapter(mAdapter);
        mActivityOrderListBinding.rvOrders.setLayoutManager(new LinearLayoutManager(this));
    }

    private void prepareData() {
        ViewModelProviders.of(this)
                .get(OrderListViewModel.class)
                .getOrders()
                .observe(this, orders -> {
                    if (orders.size() > 0) {
                        mActivityOrderListBinding.tvEmptyView.setVisibility(View.GONE);
                        mActivityOrderListBinding.rvOrders.setVisibility(View.VISIBLE);

                        mOrderList.addAll(orders);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mActivityOrderListBinding.tvEmptyView.setVisibility(View.VISIBLE);
                        mActivityOrderListBinding.rvOrders.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void sendMessage(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton("OK", null)
                .show();
    }
}
