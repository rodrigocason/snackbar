package com.rcason.snackbarapp.ui.onsale;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.rcason.snackbarapp.BR;
import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ActivityOnsaleBinding;
import com.rcason.snackbarapp.domain.entity.OnSaleItem;
import com.rcason.snackbarapp.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class OnSaleActivity extends BaseActivity<ActivityOnsaleBinding, OnSaleViewModel> implements OnSaleNavigator {

    @Inject
    OnSaleViewModel mOnSaleViewModel;

    ActivityOnsaleBinding mActivityOnsaleBinding;

    List<OnSaleItem> mList = new ArrayList<>();;
    private OnSaleAdapter mAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, OnSaleActivity.class);
        return intent;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_onsale;
    }

    @Override
    public OnSaleViewModel getViewModel() {
        return mOnSaleViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataBinding();
        setToolbar();
        setUpRecyclerView();
        prepareData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initDataBinding() {
        mActivityOnsaleBinding = getViewDataBinding();
        mActivityOnsaleBinding.setViewModel(mOnSaleViewModel);
    }

    private void setToolbar() {
        setSupportActionBar(mActivityOnsaleBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mActivityOnsaleBinding.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        mActivityOnsaleBinding.toolbar.setTitleTextColor(getColor(R.color.white));
    }

    private void setUpRecyclerView() {

        mAdapter = new OnSaleAdapter(this, mList);
        mActivityOnsaleBinding.rvOffers.setAdapter(mAdapter);
        mActivityOnsaleBinding.rvOffers.setLayoutManager(new LinearLayoutManager(this));
    }

    private void prepareData() {
        ViewModelProviders.of(this)
                .get(OnSaleViewModel.class)
                .getOnSaleItems()
                .observe(this, onSaleItems -> {
                    mList.addAll(onSaleItems);
                    mAdapter.notifyDataSetChanged();
                });
    }
}
