package com.rcason.snackbarapp.ui.onsale;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rodrigo on 06/06/2018.
 */

@Module
public class OnSaleActivityModule {

    @Provides
    OnSaleViewModel provideOnSaleViewModel() {
        return new OnSaleViewModel();
    }
}
