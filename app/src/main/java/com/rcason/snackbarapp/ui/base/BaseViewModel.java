package com.rcason.snackbarapp.ui.base;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;

import java.lang.ref.WeakReference;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class BaseViewModel<N> extends ViewModel {

    public final ObservableBoolean mIsLoading = new ObservableBoolean(false);

    private WeakReference<N> mNavigator;

    public BaseViewModel() {

    }

    public N getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }

    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.set(isLoading);
    }
}


