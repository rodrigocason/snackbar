package com.rcason.snackbarapp.ui.orderlist;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rcason.snackbarapp.BR;
import com.rcason.snackbarapp.domain.entity.Order;

import java.util.Locale;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class ItemOrderListViewModel extends BaseObservable {

    private Order mOrder;

    private Context mContext;

    public ItemOrderListViewModel(Context context, Order order) {
        this.mContext = context;
        this.mOrder = order;
        this.mHasExtras.set(order.getExtras().size() > 0);
    }

    public final ObservableBoolean mHasExtras = new ObservableBoolean(false);

    public String getSandwichName() {
        return mOrder.getSandwich() != null ? mOrder.getSandwich().getName() : "";
    }


    public String getId() {
        return String.valueOf(mOrder.id);
    }

    public String getPrice() {
        return String.format(Locale.US, "Preço: R$ %.2f", Float.valueOf(mOrder.getPrice().toString()));
    }

    public String getIngredients(){
        return String.format("Ingredientes: %s", mOrder.getSandwich().getIngredientListDescription());
    }
    public String getExtras(){
        return mOrder.getExtras().size()> 0 ? String.format("Extras: %s", mOrder.getExtraIngredientListDescription()) : "";
    }
    public ObservableBoolean getHasExtras() {
        return mHasExtras;
    }

    @Bindable
    public String getImage() {
        return mOrder.getSandwich().getImage();
    }

    public void setImage(String path) {
        mOrder.getSandwich().setImage(path);
        notifyPropertyChanged(BR.image);
    }

    // Loading Image using Glide Library.
    @BindingAdapter("image")
    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).
                load(url).
                into(imageView);
    }

    public void setOrderItem(Order order) {
        this.mOrder = order;
        notifyChange();
    }
}
