package com.rcason.snackbarapp.ui.sandwich;

import android.view.View;

import com.rcason.snackbarapp.domain.entity.Sandwich;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public interface RecyclerViewClickListener {

    void onClick(View view, Sandwich sandwich);
}
