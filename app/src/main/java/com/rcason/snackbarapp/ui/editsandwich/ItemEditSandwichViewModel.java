package com.rcason.snackbarapp.ui.editsandwich;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rcason.snackbarapp.BR;
import com.rcason.snackbarapp.domain.entity.Ingredient;
import com.rcason.snackbarapp.domain.entity.Sandwich;

import java.util.Locale;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public class ItemEditSandwichViewModel extends BaseObservable {

    private Sandwich mSandwich;
    private Ingredient mIngredient;

    private Context mContext;

    public ItemEditSandwichViewModel(Context context, Sandwich sandwich, Ingredient ingredient) {
        this.mContext = context;
        this.mSandwich = sandwich;
        this.mIngredient = ingredient;
    }

    public String getName() {
        return mIngredient.getName();
    }

    public String getPrice() {
        return String.format(Locale.US, "R$ %.2f", Float.valueOf(mIngredient.getPrice().toString()));
    }

    @Bindable
    public String getImage() {
        return mIngredient.getImage();
    }

    public void setImage(String path) {
        mIngredient.setImage(path);
        notifyPropertyChanged(BR.image);
    }

    // Loading Image using Glide Library.
    @BindingAdapter("image")
    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).
                load(url).
                into(imageView);
    }


}
