package com.rcason.snackbarapp.ui.sandwich;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.rcason.snackbarapp.data.remote.ApiFactory;
import com.rcason.snackbarapp.data.remote.ingredient.IngredientResponse;
import com.rcason.snackbarapp.data.remote.ingredient.IngredientService;
import com.rcason.snackbarapp.data.remote.orderlist.OrderService;
import com.rcason.snackbarapp.data.remote.sandwich.SandwichResponse;
import com.rcason.snackbarapp.data.remote.sandwich.SandwichService;
import com.rcason.snackbarapp.domain.entity.Ingredient;
import com.rcason.snackbarapp.domain.entity.Sandwich;
import com.rcason.snackbarapp.ui.base.BaseViewModel;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static io.reactivex.Observable.fromIterable;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class SandwichViewModel extends BaseViewModel<SandwichNavigator> {

    public SandwichViewModel() {

    }

    private MutableLiveData<List<Sandwich>> mMutableLiveDataSandwichesList;
    private List<Sandwich> mSandwichesList;

    MutableLiveData<List<Sandwich>> getSandwiches() {
        if (mMutableLiveDataSandwichesList == null) {
            mMutableLiveDataSandwichesList = new MutableLiveData<>();
            //fetchSandwiches();
            fetchAndZipSandwiches();
        }
        return mMutableLiveDataSandwichesList;
    }

    private void fetchAndZipSandwiches() {
        zipList().subscribe(listOfSandwiches());
    }

    private void fetchSandwiches() {

        setIsLoading(true);

        SandwichService sandwichService = ApiFactory.createService(SandwichService.class);
        Observable<List<SandwichResponse>> observable = sandwichService.getAllSandwiches();
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<SandwichResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("SandwichViewModel", "onSubscribe");
                    }

                    @Override
                    public void onNext(List<SandwichResponse> sandwiches) {
                        //mSandwichesList.setValue(sandwiches);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("SandwichViewModel", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("SandwichViewModel", "onComplete");
                        setIsLoading(false);
                    }
                });
    }


    private Observable<ZipResult> zipList() {

        return Observable.zip(getListOfSandwiches(), getListOfIngredients(), new BiFunction<List<SandwichResponse>, List<IngredientResponse>, ZipResult>() {
            @Override
            public ZipResult apply(@NonNull List<SandwichResponse> sandwiches, @NonNull List<IngredientResponse> ingredients) throws Exception {
                return new ZipResult(sandwiches, ingredients);
            }
        });
    }

    private Observable<List<SandwichResponse>> getListOfSandwiches() {
        return ApiFactory.createService(SandwichService.class).getAllSandwiches()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<List<IngredientResponse>> getListOfIngredients() {
        return ApiFactory.createService(IngredientService.class).getAllIngredients()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<ZipResult> listOfSandwiches() {
        return new Consumer<ZipResult>() {

            @Override
            public void accept(ZipResult response) throws Exception {

                final HashMap<Integer, Ingredient> hash = fromIterable(response.getIngredients()).collectInto(
                        new HashMap<Integer, Ingredient>(), (BiConsumer<HashMap<Integer, Ingredient>, IngredientResponse>) (map, i) -> map.put(i.id, new Ingredient(i.id, i.name, new BigDecimal(i.price.toString()), i.image))).blockingGet();

                List<Sandwich> result = fromIterable(response.getSandwiches()).map(new Function<SandwichResponse, Sandwich>() {

                    @Override
                    public Sandwich apply(@NonNull SandwichResponse i) throws Exception {
                        final Sandwich sandwich = new Sandwich(i.id, i.name, Collections.emptyList(), i.image);

                        fromIterable(i.ingredients).blockingForEach(new Consumer<Integer>() {

                            @Override
                            public void accept(Integer id) throws Exception {
                                try {
                                    Ingredient ingredient = hash.get(id);
                                    sandwich.addIngredient(ingredient);
                                } catch (Exception e) {
                                    String x = e.getMessage();
                                }
                            }

                        });

                        return sandwich;
                    }

                }).toList().blockingGet();

                mMutableLiveDataSandwichesList.setValue(result);

            }
        };
    }

    private class ZipResult {

        private List<SandwichResponse> sandwich;
        private List<IngredientResponse> ingredients;

        public ZipResult(List<SandwichResponse> sandwich, List<IngredientResponse> ingredients) {
            this.sandwich = sandwich;
            this.ingredients = ingredients;
        }

        public List<SandwichResponse> getSandwiches() {
            return sandwich;
        }

        public List<IngredientResponse> getIngredients() {
            return ingredients;
        }
    }


    public void saveOrder(int sandwichId) {

        Observable<Response<ResponseBody>> observable = ApiFactory.createService(OrderService.class).putOrder(sandwichId);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("SandwichViewModel", "onSubscribe");
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        //if (responseBodyResponse.isSuccessful()) ResponseBody responseBody = responseBodyResponse.body(); }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("SandwichViewModel", e.getMessage());
                        getNavigator().sendMessage("Erro", "Erro ao salvar Pedido!");
                    }

                    @Override
                    public void onComplete() {
                        //Log.d("SandwichViewModel", "onComplete");
                        getNavigator().sendMessage("Sucesso", "Sanduíche adicionado ao carrinho com Sucesso!");
                    }
                });
    }
}
