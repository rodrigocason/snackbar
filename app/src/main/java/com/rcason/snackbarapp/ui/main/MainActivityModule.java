package com.rcason.snackbarapp.ui.main;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rodrigo on 06/06/2018.
 */

@Module
public class MainActivityModule {

    @Provides
    MainViewModel provideMainViewModel() {
        return new MainViewModel();
    }
}
