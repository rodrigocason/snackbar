package com.rcason.snackbarapp.ui.editsandwich;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.rcason.snackbarapp.BR;
import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ActivityEditSandwichBinding;
import com.rcason.snackbarapp.domain.entity.Ingredient;
import com.rcason.snackbarapp.domain.entity.Order;
import com.rcason.snackbarapp.domain.entity.Sandwich;
import com.rcason.snackbarapp.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public class EditSandwichActivity extends BaseActivity<ActivityEditSandwichBinding, EditSandwichViewModel> implements EditSandwichNavigator, EditSandwichAdapter.OnClickListener {

    private static final String EXTRA_SANDWICH = "EXTRA_SANDWICH";


    @Inject
    EditSandwichViewModel mEditSandwichViewModel;

    ActivityEditSandwichBinding mActivityEditSandwichBinding;

    List<Ingredient> mList = new ArrayList<>();
    EditSandwichAdapter mAdapter;

    Sandwich mSandwich;
    Order mOrder;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, EditSandwichActivity.class);
        return intent;
    }

    public static Intent newIntent(Context context, Sandwich sandwich) {
        Intent intent = new Intent(context, EditSandwichActivity.class);
        intent.putExtra(EXTRA_SANDWICH, sandwich);
        return intent;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_edit_sandwich;
    }

    @Override
    public EditSandwichViewModel getViewModel() {
        return mEditSandwichViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSandwich = getIntent().getParcelableExtra(EXTRA_SANDWICH);

        initDataBinding();
        setToolbar();
        setUpRecyclerView(mSandwich);
        prepareData(mSandwich);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initDataBinding() {

        mEditSandwichViewModel.setSandwich(mSandwich);
        mEditSandwichViewModel.setNavigator(this);

        mActivityEditSandwichBinding = getViewDataBinding();
        mActivityEditSandwichBinding.setViewModel(mEditSandwichViewModel);
    }

    private void setToolbar() {
        setSupportActionBar(mActivityEditSandwichBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mActivityEditSandwichBinding.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        mActivityEditSandwichBinding.toolbar.setTitleTextColor(getColor(R.color.white));
    }

    // set up the list of user with recycler view
    private void setUpRecyclerView(Sandwich sandwich) {
        mAdapter = new EditSandwichAdapter(this, sandwich, mList, this);
        mAdapter.setOnClickListener(this);
        mActivityEditSandwichBinding.rvIngredients.setAdapter(mAdapter);
        mActivityEditSandwichBinding.rvIngredients.setLayoutManager(new LinearLayoutManager(this));
    }

    private void prepareData(Sandwich sandwich) {

        mOrder = new Order();
        mOrder.setSandwich(mSandwich);

        mActivityEditSandwichBinding.tvName.setText(sandwich.getName());
        mActivityEditSandwichBinding.tvPrice.setText(String.format(Locale.US, "R$ %.2f", Float.valueOf(sandwich.getPrice().toString())));
        mActivityEditSandwichBinding.tvIngredients.setText(sandwich.getIngredientListDescription());

        ViewModelProviders.of(this)
                .get(EditSandwichViewModel.class)
                .getIngredients()
                .observe(this, ingredients -> {
                    mList.addAll(ingredients);
                    mAdapter.notifyDataSetChanged();
                });

        mActivityEditSandwichBinding.tvTotal.setText(String.format(Locale.US, "R$ %.2f", Float.valueOf(mOrder.getPrice().toString())));
    }

    @Override
    public void onMinusClick(int position, Ingredient ingredient) {
        //mOrder.getSandwich().removeIngredient(ingredient);

        mOrder.extras.remove(ingredient);
        mEditSandwichViewModel.setIngredients(mOrder.getExtras());
        mActivityEditSandwichBinding.tvTotal.setText(String.format(Locale.US, "R$ %.2f", Float.valueOf(mOrder.getFullPrice().toString())));
    }

    @Override
    public void onPlusClick(int position, Ingredient ingredient) {
        //mOrder.getSandwich().addIngredient(ingredient);

        mOrder.extras.add(ingredient);
        mEditSandwichViewModel.setIngredients(mOrder.getExtras());
        mActivityEditSandwichBinding.tvTotal.setText(String.format(Locale.US, "R$ %.2f", Float.valueOf(mOrder.getFullPrice().toString())));
    }

    @Override
    public void sendMessage(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton("OK", null)
                .show();
    }
}
