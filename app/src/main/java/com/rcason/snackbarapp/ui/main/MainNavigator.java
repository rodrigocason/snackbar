package com.rcason.snackbarapp.ui.main;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public interface MainNavigator {

    void openSandwichesActivity();

    void openOnSaleItemsActivity();

    void openOrdersActivity();

    void openEditSandwichActivity();

}
