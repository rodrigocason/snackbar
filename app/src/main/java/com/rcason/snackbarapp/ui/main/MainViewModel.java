package com.rcason.snackbarapp.ui.main;

import com.rcason.snackbarapp.ui.base.BaseViewModel;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class MainViewModel extends BaseViewModel<MainNavigator> {

    public MainViewModel(){

    }

    public void onOpenSandwichesActivityClick(){
        getNavigator().openSandwichesActivity();
    }

    public void onOpenOnSaleItemsActivityClick(){
        getNavigator().openOnSaleItemsActivity();
    }

    public void onOpenOrdersActivityClick(){
        getNavigator().openOrdersActivity();
    }

    public void onOpenEditSandwichActivity(){
        getNavigator().openEditSandwichActivity();
    }
}
