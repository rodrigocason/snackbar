package com.rcason.snackbarapp.ui.sandwich;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public interface SandwichNavigator {


    void sendMessage(String title, String message);
}
