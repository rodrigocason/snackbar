package com.rcason.snackbarapp.ui.sandwich;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rcason.snackbarapp.BR;
import com.rcason.snackbarapp.domain.entity.Sandwich;

import java.util.Locale;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class ItemSandwichViewModel extends BaseObservable {

    private Sandwich sandwich;
    private Context mContext;

    public ItemSandwichViewModel(Context context, Sandwich sandwich) {

        this.mContext = context;
        this.sandwich = sandwich;
    }

    @Bindable
    public int getId() {
        return sandwich.getId();
    }

    public String getSandwichName() {
        return sandwich.getName();
    }

    public String getIngredients(){
        return String.format("Ingredientes: %s", sandwich.getIngredientListDescription());
    }

    public String getPrice(){
        return String.format(Locale.US, "R$ %.2f", Float.valueOf(sandwich.getPrice().toString()));
    }

    @Bindable
    public String getImage() {
        return sandwich.getImage();
    }

    public void setImage(String path) {
        sandwich.setImage(path);
        notifyPropertyChanged(BR.image);
    }

    // Loading Image using Glide Library.
    @BindingAdapter("image")
    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).
                load(url).
                into(imageView);
    }

    public void setSandwich(Sandwich sandwich) {
        this.sandwich = sandwich;
        notifyChange();
    }
}
