package com.rcason.snackbarapp.ui.sandwich;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rodrigo on 06/06/2018.
 */

@Module
public class SandwichActivityModule {

    @Provides
    SandwichViewModel provideSandwichViewModel() {
        return new SandwichViewModel();
    }
}
