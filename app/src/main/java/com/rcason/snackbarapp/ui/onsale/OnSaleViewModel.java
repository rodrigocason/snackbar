package com.rcason.snackbarapp.ui.onsale;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.rcason.snackbarapp.data.remote.ApiFactory;
import com.rcason.snackbarapp.data.remote.onsale.OnSaleService;
import com.rcason.snackbarapp.domain.entity.OnSaleItem;
import com.rcason.snackbarapp.ui.base.BaseViewModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class OnSaleViewModel extends BaseViewModel<OnSaleNavigator> {

    public OnSaleViewModel() {

    }

    private MutableLiveData<List<OnSaleItem>> mOnSaleItemsList;

    MutableLiveData<List<OnSaleItem>> getOnSaleItems() {
        if (mOnSaleItemsList == null) {
            mOnSaleItemsList = new MutableLiveData<>();
            fetchOnSaleItems();
        }
        return mOnSaleItemsList;
    }

    private void fetchOnSaleItems() {

        setIsLoading(true);

        OnSaleService onSaleService = ApiFactory.createService(OnSaleService.class);
        Observable<List<OnSaleItem>> observable = onSaleService.getAllOnSaleItemsRx();
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<OnSaleItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("OnSaleViewModel", "onSubscribe");
                    }

                    @Override
                    public void onNext(List<OnSaleItem> onSaleItems) {
                        mOnSaleItemsList.setValue(onSaleItems);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("OnSaleViewModel", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("OnSaleViewModel", "onComplete");
                        setIsLoading(false);
                    }
                });
    }
}
