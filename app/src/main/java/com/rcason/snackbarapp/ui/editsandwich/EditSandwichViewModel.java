package com.rcason.snackbarapp.ui.editsandwich;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.rcason.snackbarapp.data.remote.ApiFactory;
import com.rcason.snackbarapp.data.remote.ingredient.IngredientResponse;
import com.rcason.snackbarapp.data.remote.ingredient.IngredientService;
import com.rcason.snackbarapp.data.remote.orderlist.OrderService;
import com.rcason.snackbarapp.domain.entity.Ingredient;
import com.rcason.snackbarapp.domain.entity.Sandwich;
import com.rcason.snackbarapp.ui.base.BaseViewModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public class EditSandwichViewModel extends BaseViewModel<EditSandwichNavigator> {

    public Sandwich mSandwich;
    public List<Ingredient> mIdsIngredients;

    public EditSandwichViewModel() {

    }

    private MutableLiveData<List<Ingredient>> mList;

    MutableLiveData<List<Ingredient>> getIngredients() {
        if (mList == null) {
            mList = new MutableLiveData<>();
            fetchIngredients();
        }
        return mList;
    }

    private void fetchIngredients() {
        setIsLoading(true);

        Observable<List<IngredientResponse>> observable = ApiFactory.createService(IngredientService.class).getAllIngredients();
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<IngredientResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("OrderListViewModel", "onSubscribe");
                    }

                    @Override
                    public void onNext(List<IngredientResponse> orders) {
                        mList.setValue(wrap(orders));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("OrderListViewModel", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("OrderListViewModel", "onComplete");
                        setIsLoading(false);
                    }
                });
    }

    private List<Ingredient> wrap(List<IngredientResponse> list) {

        List<Ingredient> ingredients = new ArrayList<>();
        for (IngredientResponse i : list) {
            ingredients.add(new Ingredient(i.id, i.name, new BigDecimal(i.price.toString()), i.image));
        }
        return ingredients;
    }

    public void saveOrder() {

        if (mIdsIngredients.size() > 0) {
            ArrayList<Integer> list = new ArrayList<>();

            for (Ingredient ingredient : mIdsIngredients) {
                list.add(ingredient.getId());
            }

            Gson gson = new GsonBuilder().create();
            JsonArray ingredientsArray = gson.toJsonTree(list).getAsJsonArray();

            ApiFactory.createService(OrderService.class).putOrder(mSandwich.getId(), ingredientsArray.toString());
        } else {
            ApiFactory.createService(OrderService.class).putOrder(mSandwich.getId());

        }
        getNavigator().sendMessage("Sucesso", "Pedido salvo com sucesso");
    }

    public void setSandwich(Sandwich sandwich) {
        this.mSandwich = sandwich;
    }

    public void setIngredients(List<Ingredient> list) {
        this.mIdsIngredients = list;
    }

    @Bindable
    public String getImage() {
        return this.mSandwich.getImage();
    }

    public void setImage(String path) {
        mSandwich.setImage(path);
        //notifyPropertyChanged(BR.image);
    }

    // Loading Image using Glide Library.
    @BindingAdapter("image")
    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).
                load(url).
                into(imageView);
    }
}
