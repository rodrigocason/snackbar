package com.rcason.snackbarapp.ui.orderlist;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public interface OrderListNavigator {

    void sendMessage(String title, String message);
}
