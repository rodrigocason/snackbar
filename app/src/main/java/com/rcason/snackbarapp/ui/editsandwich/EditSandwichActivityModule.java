package com.rcason.snackbarapp.ui.editsandwich;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rodrigo on 07/06/2018.
 */

@Module
public class EditSandwichActivityModule {

    @Provides
    EditSandwichViewModel provideEditSandwichViewModel() {
        return new EditSandwichViewModel();
    }
}
