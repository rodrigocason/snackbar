package com.rcason.snackbarapp.ui.onsale;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ItemOnsaleBinding;
import com.rcason.snackbarapp.domain.entity.OnSaleItem;

import java.util.List;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class OnSaleAdapter extends RecyclerView.Adapter<OnSaleAdapter.BindingHolder> {

    private List<OnSaleItem> mList;
    private Context mContext;

    public OnSaleAdapter(Context context, List<OnSaleItem> list) {
        this.mContext = context;
        this.mList = list;
    }

    @NonNull
    @Override
    public BindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemOnsaleBinding itemOnsaleBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_onsale,
                        parent,
                        false);
        return new OnSaleAdapter.BindingHolder(itemOnsaleBinding, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull BindingHolder holder, int position) {
        OnSaleItem onSaleItem = mList.get(position);
        ItemOnsaleBinding itemOnsaleBinding = holder.binding;
        itemOnsaleBinding.setViewModel(new ItemOnSaleViewModel(mContext, onSaleItem));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        final ItemOnsaleBinding binding;

        private final Context context;

        public BindingHolder(ItemOnsaleBinding binding, Context context) {
            super(binding.getRoot());

            this.binding = binding;
            this.context = context;
        }
    }
}
