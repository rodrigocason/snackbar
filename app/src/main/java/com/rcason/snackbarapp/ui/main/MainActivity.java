package com.rcason.snackbarapp.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.rcason.snackbarapp.BR;
import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ActivityMainBinding;
import com.rcason.snackbarapp.ui.base.BaseActivity;
import com.rcason.snackbarapp.ui.editsandwich.EditSandwichActivity;
import com.rcason.snackbarapp.ui.onsale.OnSaleActivity;
import com.rcason.snackbarapp.ui.orderlist.OrderListActivity;
import com.rcason.snackbarapp.ui.sandwich.SandwichActivity;

import javax.inject.Inject;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public  class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator {

    @Inject
    MainViewModel mMainViewModel;

    ActivityMainBinding mActivityMainBinding;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        return mMainViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataBinding();
        setToolbar();

    }
    private void initDataBinding() {
        mActivityMainBinding = getViewDataBinding();
        mActivityMainBinding.setViewModel(mMainViewModel);
        mMainViewModel.setNavigator(this);
    }

    private void setToolbar() {
        setSupportActionBar(mActivityMainBinding.toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        //mActivityMainBinding.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        mActivityMainBinding.toolbar.setTitleTextColor(getColor(R.color.white));
    }

    @Override
    public void openSandwichesActivity() {
        Intent intent = SandwichActivity.newIntent(MainActivity.this);
        startActivity(intent);
    }

    @Override
    public void openOnSaleItemsActivity() {
        Intent intent = OnSaleActivity.newIntent(MainActivity.this);
        startActivity(intent);
     }

    @Override
    public void openOrdersActivity() {
        Intent intent = OrderListActivity.newIntent(MainActivity.this);
        startActivity(intent);
     }

    @Override
    public void openEditSandwichActivity() {
        Intent intent = EditSandwichActivity.newIntent(MainActivity.this);
        startActivity(intent);
    }

}
