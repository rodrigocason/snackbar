package com.rcason.snackbarapp.ui.orderlist;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ItemOrderBinding;
import com.rcason.snackbarapp.domain.entity.Order;

import java.util.List;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.BindingHolder> {

    private List<Order> mList;
    private Context mContext;

    public OrderListAdapter(Context context, List<Order> list) {
        this.mContext = context;
        this.mList = list;
    }

    @NonNull
    @Override
    public BindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemOrderBinding itemOrderBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_order,
                        parent,
                        false);

        return new BindingHolder(itemOrderBinding, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull BindingHolder holder, int position) {
        Order order = mList.get(position);

        ItemOrderBinding itemOrderBinding = holder.binding;
        itemOrderBinding.setViewModel(new ItemOrderListViewModel(mContext, order));

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        final ItemOrderBinding binding;

        private final Context context;

        public BindingHolder(ItemOrderBinding binding, Context context) {
            super(binding.getRoot());

            this.binding = binding;
            this.context = context;
        }
    }
}
