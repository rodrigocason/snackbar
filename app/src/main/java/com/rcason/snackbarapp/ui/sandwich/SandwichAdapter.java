package com.rcason.snackbarapp.ui.sandwich;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rcason.snackbarapp.R;
import com.rcason.snackbarapp.databinding.ItemSandwichBinding;
import com.rcason.snackbarapp.domain.entity.Sandwich;

import java.util.List;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class SandwichAdapter extends RecyclerView.Adapter<SandwichAdapter.BindingHolder> {

    private List<Sandwich> mList;
    private Context mContext;
    private RecyclerViewClickListener mListener;

    public SandwichAdapter(Context context, List<Sandwich> list, RecyclerViewClickListener listener) {
        this.mContext = context;
        this.mList = list;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public SandwichAdapter.BindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSandwichBinding itemSandwichBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_sandwich,
                        parent,
                        false);
        return new SandwichAdapter.BindingHolder(itemSandwichBinding, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull SandwichAdapter.BindingHolder holder, int position) {
        final Sandwich sandwich = mList.get(position);
        ItemSandwichBinding itemSandwichBinding = holder.binding;
        itemSandwichBinding.setViewModel(new ItemSandwichViewModel(mContext, sandwich));
        holder.binding.llSandwich.setOnClickListener(v -> mListener.onClick(v, mList.get(position)));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull BindingHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull BindingHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.binding.unbind();
    }


    public class BindingHolder extends RecyclerView.ViewHolder {

        final ItemSandwichBinding binding;
        private final Context context;

        public BindingHolder(ItemSandwichBinding binding, Context context) {
            super(binding.getRoot());

            this.binding = binding;
            this.context = context;
        }
    }
}
