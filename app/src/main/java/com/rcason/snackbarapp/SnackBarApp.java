package com.rcason.snackbarapp;

import com.rcason.snackbarapp.di.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class SnackBarApp extends DaggerApplication {

    private static SnackBarApp app;

    public static SnackBarApp getApp() {
        return app;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
