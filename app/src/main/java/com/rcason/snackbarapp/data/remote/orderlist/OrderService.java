package com.rcason.snackbarapp.data.remote.orderlist;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public interface OrderService {

    @GET("pedido")
    Observable<List<OrderResponse>> getAllOrders();

    @PUT("pedido/{id_sandwich}")
    Observable<Response<ResponseBody>> putOrder(@Path("id_sandwich") int id_sandwich);


    @FormUrlEncoded
    @PUT("pedido/{id_sandwich}")
    Observable<Response<ResponseBody>> putOrder(@Path("id_sandwich") int id_sandwich, @Field("extras") String extras);
}
