package com.rcason.snackbarapp.data.remote.orderlist;

import java.util.List;

/**
 * Created by Rodrigo on 08/06/2018.
 */

public class OrderResponse {

    public int id;
    public int id_sandwich;
    public List<Integer> extras;
    public String date;
}
