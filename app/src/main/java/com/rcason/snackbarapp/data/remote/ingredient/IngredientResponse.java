package com.rcason.snackbarapp.data.remote.ingredient;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public class IngredientResponse {

    public Integer id;
    public String name;
    public Double price;
    public String image;
}
