package com.rcason.snackbarapp.data.remote.sandwich;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public interface SandwichService {

    // All ===============================
    @GET("lanche")
    Observable<List<SandwichResponse>> getAllSandwiches();



    //ById ===============================
    @GET("lanche/{id}")
    Observable<SandwichResponse> getSandwichById(@Path("id") int id);
    //      ===============================
    //@GET
    //Observable<List<Sandwich>> fetchSandiches(@Url String url);
    //Observable<List<Sandwich>> fetchSandiches(@Url String url);

}
