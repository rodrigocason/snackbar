package com.rcason.snackbarapp.data.remote.ingredient;

import com.rcason.snackbarapp.domain.entity.Ingredient;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public interface IngredientService {


    @GET("ingrediente")
    Observable<List<IngredientResponse>> getAllIngredients();


    @GET("ingrediente/de/{idlanche}")
    Observable<List<Ingredient>> getIngredientsOfSandwich(int sandwichId);

}
