package com.rcason.snackbarapp.data.remote.onsale;

import com.rcason.snackbarapp.domain.entity.OnSaleItem;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public interface OnSaleService {

    @GET("promocao")
    Observable<List<OnSaleItem>> getAllOnSaleItemsRx();

    @GET("promocao")
    Call<List<OnSaleItem>> getAllOnSaleItems();
}
