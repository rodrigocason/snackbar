package com.rcason.snackbarapp.data.remote.sandwich;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodrigo on 07/06/2018.
 */

public class SandwichResponse {

    @SerializedName("id") public Integer id;
    @SerializedName("name") public String name;
    @SerializedName("image") public String image;
    @SerializedName("ingredients") public List<Integer> ingredients = new ArrayList<>();

    public SandwichResponse() {}

    public SandwichResponse(Integer id, String name, String image, List<Integer> ingredients) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.ingredients = ingredients;
    }
}
