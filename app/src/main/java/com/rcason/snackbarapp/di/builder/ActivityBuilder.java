package com.rcason.snackbarapp.di.builder;

import com.rcason.snackbarapp.ui.editsandwich.EditSandwichActivity;
import com.rcason.snackbarapp.ui.editsandwich.EditSandwichActivityModule;
import com.rcason.snackbarapp.ui.main.MainActivity;
import com.rcason.snackbarapp.ui.main.MainActivityModule;
import com.rcason.snackbarapp.ui.onsale.OnSaleActivity;
import com.rcason.snackbarapp.ui.onsale.OnSaleActivityModule;
import com.rcason.snackbarapp.ui.orderlist.OrderListActivity;
import com.rcason.snackbarapp.ui.orderlist.OrderListActivityModule;
import com.rcason.snackbarapp.ui.sandwich.SandwichActivity;
import com.rcason.snackbarapp.ui.sandwich.SandwichActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


/**
 * Created by rodrigo.cason on 28/03/2018.
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = SandwichActivityModule.class)
    abstract SandwichActivity bindSandwichActivity();

    @ContributesAndroidInjector(modules = OnSaleActivityModule.class)
    abstract OnSaleActivity bindOnSaleActivity();

    @ContributesAndroidInjector(modules = OrderListActivityModule.class)
    abstract OrderListActivity bindOrderListActivity();

    @ContributesAndroidInjector(modules = EditSandwichActivityModule.class)
    abstract EditSandwichActivity bindEditSandwichActivity();

}
