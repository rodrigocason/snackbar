package com.rcason.snackbarapp.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class Ingredient implements Serializable {


    private Integer id;
    private String name;
    private BigDecimal price;
    private String image;

    public Ingredient() {
    }

    public Ingredient(Integer id, String name, BigDecimal price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
