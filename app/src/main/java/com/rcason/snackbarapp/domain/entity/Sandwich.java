package com.rcason.snackbarapp.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class Sandwich  implements Parcelable {

    public Sandwich() {
    }

    public Sandwich(Integer id, String name, List<Ingredient> ingredients, String image) {
        this.id = id;
        this.name = name;
        this.image = image;

        this.ingredients.addAll(ingredients);
    }

    private Integer id;
    private String name;
    private String image;
    private List<Ingredient> ingredients = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPrice(){
        BigDecimal price = BigDecimal.ZERO;

        for (Ingredient ingredient : ingredients){
            price = price.add(ingredient.getPrice());
        }

        return price;
    }

    public List<Ingredient> getIngredients() {
        return Collections.unmodifiableList(ingredients);
    }

    public String getIngredientListDescription(){
        if(ingredients.isEmpty())
            return "";

        StringBuilder builder = new StringBuilder();

        for (Ingredient ingredient : ingredients){
            builder.append(ingredient.getName());
            builder.append(", ");
        }

        int last = builder.lastIndexOf(",");
        return builder.substring(0, last);
    }

    public void addIngredient(Ingredient ingredient){
        ingredients.add(ingredient);
    }

    public void removeIngredient(Ingredient ingredient){
        ingredients.remove(ingredient);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.getName());
        dest.writeList(this.getIngredients());
        dest.writeString(this.getImage());
    }
    public static final Creator<Sandwich> CREATOR = new Creator<Sandwich>() {
        public Sandwich createFromParcel(Parcel source) {
            return new Sandwich(source);
        }

        public Sandwich[] newArray(int size) {
            return new Sandwich[size];
        }
    };

    protected Sandwich(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.ingredients = new ArrayList<>();
        in.readList(this.ingredients, Ingredient.class.getClassLoader());
        this.image = in.readString();

    }
}
