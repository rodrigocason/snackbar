package com.rcason.snackbarapp.domain.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodrigo on 06/06/2018.
 */

public class Order {

    public static final BigDecimal LIGHT_FOOD_DISCOUNT = new BigDecimal("0.1");

    public static final int NUMBER_OF_MEAT_TO_GET_DISCOUNT = 3;
    public static final int NUMBER_OF_CHEESE_TO_GET_DISCOUNT = 3;

    public Order() {

    }

    public Order(int id, Sandwich sandwich, List<Ingredient> extras) {
        this.id = id;
        this.mSandwich = sandwich;
        this.extras.addAll(extras);
    }

    public int id;
    public int mSandwichId;
    public Sandwich mSandwich;
    public List<Ingredient> extras = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSandwichId() {
        return mSandwichId;
    }

    public void setSandwichId(int sandwichId) {
        this.mSandwichId = sandwichId;
    }


    public Sandwich getSandwich() {
        return mSandwich;
    }

    public void setSandwich(Sandwich sandwich) {
        this.mSandwich = sandwich;
    }

    public List<Ingredient> getExtras() {
        return extras;
    }

    public void setExtras(List<Ingredient> extras) {
        this.extras = extras;
    }

    public BigDecimal getPrice() {

        BigDecimal normalValue = mSandwich.getPrice();
        BigDecimal customValue = BigDecimal.ZERO;

        for (Ingredient i : this.extras)
            customValue = customValue.add(i.getPrice());

        return normalValue.add(customValue);
    }

    public BigDecimal getFullPrice() {

        BigDecimal price = getPrice();

        if (hasLightDiscount()) {
            BigDecimal discount = price.multiply(LIGHT_FOOD_DISCOUNT);
            price = price.subtract(discount); // 10%
        }

        if (hasLotsOfCheese()) {
            BigDecimal priceOf = getPriceOfIngredient("Queijo");
            int number = getNumberOf("Queijo");
            int free = number / NUMBER_OF_CHEESE_TO_GET_DISCOUNT;

            BigDecimal discount = priceOf.multiply(new BigDecimal(free));
            price = price.subtract(discount);
        }

        if (hasLotsOfMeal()) {
            BigDecimal priceOf = getPriceOfIngredient("Carne");
            int number = getNumberOf("Carne");
            int free = number / NUMBER_OF_MEAT_TO_GET_DISCOUNT;

            BigDecimal discount = priceOf.multiply(new BigDecimal(free));
            price = price.subtract(discount);
        }

        return price;
    }

    public boolean hasLightDiscount() {

        boolean hasLettuce = getNumberOf("Alface") > 0;
        boolean hasBacon = getNumberOf("Bacon") > 0;

        return hasLettuce && !hasBacon;
    }

    public boolean hasLotsOfMeal() {
        return getNumberOf("Carne") >= NUMBER_OF_MEAT_TO_GET_DISCOUNT;
    }

    public boolean hasLotsOfCheese() {
        return getNumberOf("Queijo") >= NUMBER_OF_CHEESE_TO_GET_DISCOUNT;
    }

    private int getNumberOf(String type) {
        int number = 0;

        for (Ingredient ingredient : mSandwich.getIngredients()) {
            if (ingredient.getName().toLowerCase().contains(type.toLowerCase())) number++;
        }

        for (Ingredient ingredient : extras) {
            if (ingredient.getName().toLowerCase().contains(type.toLowerCase())) number++;
        }

        return number;
    }

    private BigDecimal getPriceOfIngredient(String ingredientName) {
        BigDecimal price = BigDecimal.ZERO;

        for (Ingredient ingredient : mSandwich.getIngredients()) {
            if (ingredient.getName().toLowerCase().contains(ingredientName.toLowerCase()))
                price = ingredient.getPrice(); //price.add(ingredient.getPrice());

        }

        for (Ingredient ingredient : extras) {
            if (ingredient.getName().toLowerCase().contains(ingredientName.toLowerCase()))
                price = ingredient.getPrice(); //price.add(ingredient.getPrice());
        }

        return price;
    }

    public void addIngredient(Ingredient ingredient){
        extras.add(ingredient);
    }

    public String getExtraIngredientListDescription(){
        if(extras.isEmpty())
            return "";

        StringBuilder builder = new StringBuilder();

        for (Ingredient extraIngredient : extras){
            builder.append(extraIngredient.getName());
            builder.append(", ");
        }

        int last = builder.lastIndexOf(",");
        return builder.substring(0, last);
    }

}
